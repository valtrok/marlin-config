# Marlin configuration

## Setup

### Hardware

- Base printer: Anet A8
- Motherboard: RAMPS 1.6
- LCD: RepRapDiscount Full Graphic Smart Controller (12864)
- Stepper drivers: DRV8825
- Extruder: Stock motor with BMG dual drive
- Hotend: E3D v6 clone
- Autolevel probe: BLTouch clone

### Software

- Marlin version: 2.0.x
